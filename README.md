# Final Project Personal Finance - Group 5 
**In Bitbucket, we created a fork:**
https://bitbucket.org/BuckRogers_1/devops_g5/src/master/
_____________________
**Work Division**

We all worked together in meetings.

Some had been left with more investigative activities, others in which their machines were the most operational.

For those in which computers were the operating machines, namely Alexandre and Duarte, 
the group recognizes their hard work and great performance, mainly because they had to spend the keys 
to write "Vagrant destroy" thousands of times, a big thanks to them!
___________________________________
### 1- Objectives:
The objective of this work is to implement a pipeline in Jenkins for our Personal Finance Project 
where we should apply:

#### 1.1 - Infrastructure as Code:
It should be possible to reproduce your solution (and alternative) in a system with Vagrant and a clone of your repository 
(i.e., the scripts and code of your repository).

#### 1.2 - Git / Bitbucket.

This is a team project, create tasks and issues for all team members.
Use branches to develop specific features of the solution.

#### 1.3 - Vagrant:
Use Vagrant to virtualize all the parts of the solution:

##### - One VM with Jenkins, Ansible and Docker

*Build Stages of the Pipeline for Jenkins:*
- Assemble the application; 
- Generate and publish javadoc in Jenkins; 
- Execute tests (unit and integration) 
- Publish its results in Jenkins (including code coverage)
- Publish the distributable artifacts in Jenkins (e.g., jar ﬁles) 

*Build Stages of the Pipeline for Docker:*
- Create 2 docker images (app + db) and publish them in docker hub 

*Build Stages of the Pipeline for Ansible:*
- Deploy and configure the application and its database - we used PostgreSQL

##### - One VM to install and configure the Personal Finance web application 

##### - One VM to install and configure the database of the application
__________________________________
#### 1.4 - Requirements: 

To complete this assigment we used the following software: 

Version Control: Git 

Repository: Bitbucket 

IDE: IntelliJ IDEA 

*Below we describe all the tasks, files and folders that we need to perform in order to achieve all the proposed objectives.*
____________________
### 2 - Deployment

#### 2.1 - SSH folder:
In command line we run: 

	ssh-keygen


This will generate two files, that we added to the shared folder in our repository, so the vagrant can copy the file to 
inside the folder .ssh in the virtual machine. 
We know that this isn't the best practice, in a security level, because the private key is available in the repository.

The following files have been added to this folder: 

***- id_rsa*** - private key

***- id_rsa.pub*** - public key
___________________________________________
#### 2.2 - Vagrantfile - working without Ansible

This file was created to configure how the VMs are created.

General settings applied to all VMs:
    
    # See: https://manski.net/2016/09/vagrant-multi-machine-tutorial/
    # for information about machine names on private network
    Vagrant.configure("2") do |config|
    
      config.vm.box = "envimation/ubuntu-xenial"
   
Open a shell and insert instructions of commands automatically. 
In this case, applies to each VM's Configuration provisioning step:
    
      # This provision is common for both VMs
      config.vm.provision "shell", inline: <<-SHELL
        sudo apt-get update -y
        sudo apt-get install iputils-ping -y
        sudo apt-get install -y avahi-daemon libnss-mdns
        sudo apt-get install -y unzip
        sudo apt-get install openjdk-8-jdk-headless -y
        # ifconfig
      SHELL
      
This refers to the creation of a VM that install and configure a database (db) with H2:

     #============
       # Configurations specific to the database VM
       config.vm.define "db" do |db|
         db.vm.box = "envimation/ubuntu-xenial"
         db.vm.hostname = "db"
         db.vm.network "private_network", ip: "192.168.33.11"
     
         # We want to access H2 console from the host using port 8082
         # We want to connet to the H2 server using port 9092
         db.vm.network "forwarded_port", guest: 8082, host: 8082
         db.vm.network "forwarded_port", guest: 9092, host: 9092
     
         # We need to download H2
         db.vm.provision "shell", inline: <<-SHELL
           wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
         SHELL
     
         # The following provision shell will run ALWAYS so that we can execute the H2 server process
         # This could be done in a different way, for instance, setiing H2 as as service, like in the following link:
         # How to setup java as a service in ubuntu: http://www.jcgonzalez.com/ubuntu-16-java-service-wrapper-example
         #
         # To connect to H2 use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
         db.vm.provision "shell", :run => 'always', inline: <<-SHELL
           java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
         SHELL
       end

This refer to the creation of a VM, that install and configure Jenkins and Docker:

	  #============
        config.vm.define "jenkins" do |jenkins|
          jenkins.vm.box = "envimation/ubuntu-xenial"
          jenkins.vm.hostname = "jenkins"
          jenkins.vm.network "private_network", ip: "192.168.33.13"
          jenkins.vm.network "forwarded_port", guest: 8080, host: 8085
      
          jenkins.vm.provider "virtualbox" do |vb|
            vb.gui = false
            vb.cpus = 2
            vb.memory = "4096"
          end
      
          config.ssh.username = "vagrant"
          config.ssh.password = "vagrant"
      
          jenkins.vm.provision "shell", inline: <<-SHELL
            sudo apt-get install -y apt-transport-https
            wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
            sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
            sudo apt-get update
            sudo apt-get install -y git
            sudo apt-get update
            sudo apt-get install -y\
            >     apt-transport-https \
            >     ca-certificates \
            >     curl \
            >     gnupg-agent \
            >     software-properties-common
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
            sudo add-apt-repository \
            >    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
            >    $(lsb_release -cs) \
            >    stable"
            sudo apt-get update
            sudo apt-get install docker-ce docker-ce-cli containerd.io
            sudo apt-get install -y jenkins
            sudo service jenkins start
            sleep 1m
            JENKINSPWD=$(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)
          SHELL
        end
	  
Last, but not least, we have the creation of the last VM, where we run our application: 

        #============
      # Configurations specific to the webserver VM
      config.vm.define "web" do |web|
    
        web.vm.box = "envimation/ubuntu-xenial"
        web.vm.hostname = "web"
        web.vm.network "private_network", ip: "192.168.33.12"
    
        config.vm.synced_folder ".", "/shared"
    
        # We set more ram memmory for this VM
        web.vm.provider "virtualbox" do |v|
          v.memory = 1024
        end
    
        # We want to access tomcat from the host using port 8080
        web.vm.network "forwarded_port", guest: 8080, host: 8080
    
        config.ssh.username = "vagrant"
        config.ssh.password = "vagrant"
    
       web.vm.provision "shell", privileged: false, inline: <<-SHELL
          sudo apt-get install -y ssh
          sudo apt-get install git -y
          sudo apt-get install nodejs -y
          sudo apt-get install npm -y
          sudo ln -s /usr/bin/nodejs /usr/bin/node
          sudo apt install tomcat8 -y
          sudo apt install tomcat8-admin -y
          # If you want to access Tomcat admin web page do the following:
          # Edit /etc/tomcat8/tomcat-users.xml
          # uncomment tomcat-users and add manager-gui to tomcat user
    
          #install maven
          sudo apt-get install maven -y
    
          #Clean .ssh
          rm -rf .ssh
          mkdir .ssh
    
          #Get private key
    
          cp /shared/ssh/id_rsa /home/vagrant/.ssh/
          sudo chmod 700 /home/vagrant/.ssh/id_rsa
    
          cp /shared/ssh/id_rsa.pub /home/vagrant/.ssh/
          
          ssh-keyscan -t rsa bitbucket.org >> /home/vagrant/.ssh/known_hosts
          cat /shared/ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
    
          # Clone repository
          git clone git@bitbucket.org:BuckRogers_1/devops_g5.git
          cd devops_g5
          mvn compile war:war
          # To deploy the war file to tomcat8 do the following command:
          sudo cp /home/vagrant/devops_g5/target/training-1.0-SNAPSHOT.war /var/lib/tomcat8/webapps
      SHELL
    
      end
    
    end
____________________
#### 2.3 - Postgres
    
Postgres is the database we use. 
During the pipeline, namely on the Test Stage, a VM with Postegres already installed is used for storing the data.
This is done initially in Vagrant, in the VM "db" creation. During the deployment with Ansible to the production db, 
another instance of Postgres is applied ("db2").

***- pg_hba.conf*** - to use this file we modified it, adding # IPv4 local connections.
This will open all the remote IPv4 connections.

***- postgresql.conf*** - to use this file we modified the line with the "listen_addresses=localhost" to "listen_addresses=*".
____________________
#### 2.4 - .mvn/wrapper folder

This folder was created to be used when called by the vagrant to ensure that the Maven's versions used are correct and the same on all machines.
This makes it possible to use Maven on a computer that does not have it installed.

The following files have been added to this folder:

***- MavenWrapperDownloader.java***

***- maven-wrapper.properties***

____________________
And this are the Executable file, for all OS types: 

***- mvnw file***

***- mvnw.cmd file***

____________________
#### 2.5 - Vagrant SetUp folder 

This folder was automatically created  when we run on command line:

		 vagrant up

The following file was added to this folder:

***bootstrap. sh***
___________________________________
#### 2.6 - Dockerfile - web 

We use to build a container according to the specifications placed here.
This Dockerfile is for web so we assure that the container have tomcat, git and node:

    FROM tomcat
    
    ARG SSH_PRIVATE_KEY
    
    ARG SSH_PUBLIC_KEY
     
    RUN apt-get update -y
     
    RUN apt-get install -f
     
    RUN apt-get install git -y
     
    RUN apt-get install nodejs -y
     
    RUN apt-get install npm -y
    
    # Authorize SSH Host
    RUN mkdir -p /root/.ssh && \
        chmod 0700 /root/.ssh && \
        ssh-keyscan -t rsa bitbucket.org > /root/.ssh/known_hosts
    
    # Add the keys and set permissions
    RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa && \
        echo "${SSH_PUBLIC_KEY}" > /root/.ssh/id_rsa.pub && \
        chmod 600 /root/.ssh/id_rsa && \
        chmod 600 /root/.ssh/id_rsa.pub && \
        cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
    
    RUN echo ${SSH_PRIVATE_KEY}
    
    RUN cat /root/.ssh/id_rsa
    
    RUN mkdir -p /tmp/build
    
    WORKDIR /tmp/build/
    
    RUN git clone git@bitbucket.org:BuckRogers_1/devops_g5.git
    
    WORKDIR /tmp/build/devops_g5/
    
    RUN chmod +x mvnw
    
    RUN ./mvnw compile war:war
    
    RUN cp target/training-1.0-SNAPSHOT.war  /usr/local/tomcat/webapps/
    
    RUN rm /root/.ssh/id_rsa
    
    RUN rm /root/.ssh/id_rsa.pub
    
    EXPOSE 8080
__________________________
#### 2.7 - Dockerfile - db  

We use to build a container according to the specifications placed here.
This Dockerfile is for H2 db, so we assure that the container have the right access to our db.

    FROM ubuntu
    
    RUN apt-get update && \
      apt-get install -y openjdk-8-jdk-headless && \
      apt-get install unzip -y && \
      apt-get install wget -y
    
    RUN mkdir -p /usr/src/app
    
    WORKDIR /usr/src/app/
    
    RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
    
    EXPOSE 8082
    EXPOSE 9092
    
    CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
______________________
#### 2.8 - jenkinsfile

When you configure Jenkins, in browser, to work with Vagrantfile and Ansible you must install the following plugins:
- HTML publisher
- Jacoco
- Docker (pipeline, build step, API, plugin)
- Ansible (Tower, plugin)

If you only work with Vagrantfile you don't need to install the Ansible's plugin.

Execute the pipeline, to build the different stages that we want: 

**Stages**:

Begin with the configuration of the ssh, then we enter in the the pipeline:

*Note: Everyone of group 5 defined its own credentials Id and docker hub credentials, for accessing into Bitbucket repository 
and Docker Hub in that order.*

    def PKEY
    def PUKEY
        
        node {
                script{
                    git credentialsId: 'Bitbucket', url:'https://BuckRogers_1@bitbucket.org/BuckRogers_1/devops_g5.git'
                }
        }
        
        node {
          PKEY = sh(returnStdout: true, script: 'cat ssh/id_rsa')
          PUKEY = sh(returnStdout: true, script: 'cat ssh/id_rsa.pub')
        }

**1. Checkout:** Jenkins validate the Repository and it's credentials and get all the files in it:
    
    pipeline {
            agent any
            stages {
                        stage('Checkout') {
                    steps {
                        echo 'Checking out...'
                        git credentialsId: 'Bitbucket', url:'https://BuckRogers_1@bitbucket.org/BuckRogers_1/devops_g5.git'
                    }
                }

**2. Assemble:** In order to prevent errors in the following 'Test' stage, we always remove the file with the results, 
thus adding "./mvnw clean war:war", in order to ensure that when we run the next stage it does not have any previous files there:

        stage('Assemble') {
                            steps {
                                echo 'Assembling...'
                                sh 'chmod +x mvnw'
                                sh './mvnw clean war:war'
                        }
                    }

**3. Test:** run the tests associated to the program and put the results on file training-1.0-SNAPSHOT.war on jenkins:

       stage('Test') {
                    steps {
                        echo 'Running tests...'
                        sh './mvnw test'
                        junit 'target/surefire-reports/*.xml'
                        publishHTML (target: [
                          allowMissing: false,
                          alwaysLinkToLastBuild: true,
                          keepAll: true,
                          reportDir: 'target/site/jacoco',
                          reportFiles: 'index.html',
                          reportName: "Tests Coverage"
                        ])
                    }
                }

**4. Javadoc:** generate the javadoc of the project and archive the results in a junit-formatted way:

      stage('Javadoc') {
                steps {
                    echo 'Generating Javadoc...'
                    sh './mvnw javadoc:javadoc'
                    publishHTML (target: [
                      allowMissing: false,
                      alwaysLinkToLastBuild: true,
                      keepAll: true,
                      reportDir: 'target/site/apidocs',
                      reportFiles: 'index.html',
                      reportName: "Javadoc"
                    ])
                }
            }

**5. Archive:** archive the files in the location we want:

      stage('Archiving') {
                    steps {
                        echo 'Archiving...'
                        archiveArtifacts 'target/*.war'
                    }
                }

**6. Docker:** generate docker images (web + db) with Tomcat and DB and publish them in docker hub:

       stage('Docker Image') {
               steps {
                   echo 'Creating and publishing Docker Image...'
                    script{
                        docker.withRegistry('https://index.docker.io/v1/','dockerhub'){
                            docker.build("1060237/switch_devops:${env.BUILD_ID}","--build-arg SSH_PRIVATE_KEY='${PKEY}' --build-arg SSH_PUBLIC_KEY='${PUKEY}' .").push()
                        }
                    }
                }
            }

**7. Ansible:** to deploy and configure the application and its database:

       stage('Deploy db') {
              steps{
                  echo 'Deploying postgres db'
                    ansiblePlaybook(credentialsId: 'vagrant', inventory: 'hosts', playbook: 'playbook1.yml')
                }
            }
            stage('Deploy web') {
                steps{
                    echo 'Deploying G5 project to web VM'
                    ansiblePlaybook(credentialsId: 'vagrant', inventory: 'hosts', playbook: 'playbook2.yml')
                }
            }
        }
    }
___________________________________________
**NOTE**
So far we have done the work in a version that we only work with Vagrantfile.
Now we adapted our Vagrantfile and put some of responsibilities in playbooks (1 and 2) to work with ansible.
This is another way to do the work, that automatize the process and deploy.

Besides the differences in Vagrantfile, and the addition of new files, we should install one more plugin 
when configuring Jenkins (Ansible plugin).

Below we show the Vagrantfile adapted to Ansible, the playbooks1 and 2 and the ansible.cfg file.
___________________________________________
#### 2.9 - Vagrantfile - working with Ansible

Here you will see the Vagrantfile adapted to work with ansible,where we created 4 VMs: 
two DB (one for production and another for tests), one for jenkins/ansible and one for web.

General settings applied to all VMs:

    # See: https://manski.net/2016/09/vagrant-multi-machine-tutorial/
    # for information about machine names on private network
    Vagrant.configure("2") do |config|
    
      config.vm.box = "envimation/ubuntu-xenial"
      config.vm.synced_folder ".", "/shared", mount_options: ["dmode=775,fmode=600"]
    
      config.ssh.username = "vagrant"
      config.ssh.password = "vagrant"

Open a shell and insert instructions of commands automatically. 
In this case, applies to each VM's Configuration provisioning step:
    
      # This provision is common for both VMs
      config.vm.provision "shell", inline: <<-SHELL
        sudo apt-get update -y
        sudo apt-get install iputils-ping -y
        sudo apt-get install python3 --yes
        sudo apt-get install -y avahi-daemon libnss-mdns
        sudo apt-get install -y unzip
        # ifconfig
      SHELL
      
This refers to the creation of two VMs, one install and configure an empty database with Postgres (db2) and another (db) so we can run the tests:
     
    #============
      # Configurations specific to the database VM
        config.vm.define "db2" do |db|
        db.vm.box = "envimation/ubuntu-xenial"
        db.vm.hostname = "db2"
        db.vm.network "private_network", ip: "192.168.33.10"
      end
    
      #============
      # Configurations specific to the database VM
      config.vm.define "db" do |db|
        db.vm.box = "envimation/ubuntu-xenial"
        db.vm.hostname = "db"
        db.vm.box = "envimation/ubuntu-xenial"
    
        db.vm.provision "shell", privileged: false, inline: <<-SHELL
    
        wget -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
        sudo apt-get install postgresql postgresql-contrib
        sudo apt-get remove postgresql-9.1 postgresql-contrib-9.1 postgresql-client-9.1
        sudo apt-get install -y postgresql postgresql-contrib
        sudo apt-get update -y
        sudo apt-get install -y libpq-dev
    #	sudo sed -i "s/port = 5433/port = 5432/" /etc/postgresql/9.5/main/postgresql.conf
        sudo cp /shared/postgres/pg_hba.conf /etc/postgresql/9.5/main && sudo cp /shared/postgres/postgresql.conf /etc/postgresql/9.5/main
        sudo -u postgres psql -c "ALTER USER postgres WITH ENCRYPTED PASSWORD 'postgres';"
        sudo /etc/init.d/postgresql restart
    
       SHELL
      
      # PostgreSQL Server port forwarding
      db.vm.network "forwarded_port", guest: 5432, host: 5432
      db.vm.network "private_network", ip: "192.168.33.11"
    end

This refers to the creation of a VM, that install and configure Jenkins, Ansible and Docker:
    
      #============
        config.vm.define "jenkins" do |jenkins|
          jenkins.vm.box = "envimation/ubuntu-xenial"
          jenkins.vm.hostname = "jenkins"
          jenkins.vm.network "private_network", ip: "192.168.33.13"
          jenkins.vm.network "forwarded_port", guest: 8080, host: 8085
      
          jenkins.vm.provider "virtualbox" do |vb|
            vb.gui = false
            vb.cpus = 2
            vb.memory = "4096"
          end
      
          config.ssh.username = "vagrant"
          config.ssh.password = "vagrant"
      
          jenkins.vm.provision "shell", inline: <<-SHELL
            sudo apt-get install -y --no-install-recommends apt-utils
            sudo apt-get install software-properties-common --yes
            sudo apt-get install openjdk-8-jdk-headless -y
            sudo apt-add-repository --yes --u ppa:ansible/ansible
            sudo apt-get install ansible --yes
            sudo apt-get install -y apt-transport-https
            wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
            sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
            sudo apt-get update
            sudo apt-get install -y git
            sudo apt-get update
            sudo apt-get install -y postgresql-client
            sudo apt-get install -y\
            >     apt-transport-https \
            >     ca-certificates \
            >     curl \
            >     gnupg-agent \
            >     software-properties-common
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
            sudo add-apt-repository \
            >    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
            >    $(lsb_release -cs) \
            >    stable"
            sudo apt-get update
            sudo apt-get install docker-ce docker-ce-cli containerd.io
            sudo apt-get install -y jenkins
            sudo service jenkins start
            sleep 1m
            JENKINSPWD=$(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)
          SHELL
        end

Last, but not least, we have the creation of the last VM, where we run our application: 
    
      #  #============
       config.vm.define "web" do |web|
          web.vm.box = "envimation/ubuntu-xenial"
          web.vm.hostname = "web"
          web.vm.network "private_network", ip: "192.168.33.12"
      
      # We want to access tomcat from the host using port 8080
          web.vm.network "forwarded_port", guest: 8080, host: 8080
      
       # We set more ram memmory for this VM
          web.vm.provider "virtualbox" do |v|
            v.memory = 1024
          end
        end
      end
____________________________________
#### 2.10 - playbook1.yml, playbook2.yml files 

They were created to install what we need to run this project using ansible. 

* **playbook1** is responsible for installing PostgreSQL, libpq, python and copying configuration files to Postegres "db2".

* **playbook2** is responsible for installing jdk-8 in web and db VMs and tomcat8 , git, ssh, nodejs, npm and maven only in web VM. 
Facilitate the ssh key to access bitbucket, authorize hosts, clone our repository, 
compile project, originate war file and deploy the application.
_____________________________________
#### 2.11 - ansible.cfg file 

This file is used to define the name of the user of Ansible and its hosts inventory:

	[defaults]
    inventory = hosts
    remote_user = vagrant
______________________________________
#### 2.12 - hosts file

This file is used to define all the VMs present in Ansible tasks, making the necessary logins to VMs.

    [otherservers]
    db2 ansible_ssh_host=192.168.33.10 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_password=vagrant ansible_ssh_private_key_file=/shared/.vagrant/machines/db2/virtualbox/private_key
    web ansible_ssh_host=192.168.33.12 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_password=vagrant ansible_ssh_private_key_file=/shared/.vagrant/machines/web/virtualbox/private_key
________________

#### 3 - Conclusion

To conclude this work you must:

##### 3.1- Clone the repo to a folder you want

##### 3.2- Prepare the VMs

3.2.1 - To create the VMs we run in command line:

    vagrant up
    
This will run the provision script to prepare the VMs, call the hypervisor and effectively create all the 4 VMs we defined, 
with the tools, setups the network and shared folders, and setups SSH keys to logon to the VMs.

##### 3.3- Build the project

##### 3.4- See the results

We used Postman to ensure that our application running on "web" VM was using "db2" for persistence after their deployment. 
_____________________________________
## 4 - ALTERNATIVE
____________________________________
**In Bitbucket:**
https://1191756@bitbucket.org/1191756/devops_g5_alt_survivor.git 
___________________________________

We opted to create a new repository in order to avoid ssh access keys configuration issues.

We made the conversion from maven to gradle using:

    gradle init
    
 This generate a file build.gradle that contained all the dependencies that were imported from pom.xml file.
 
 The lombok file was also modified:
 
     compileOnly 'org.projectlombok:lombok:1.18.12'
     annotationProcessor 'org.projectlombok:lombok:1.18.12'
     testCompileOnly 'org.projectlombok:lombok:1.18.12'
     testAnnotationProcessor 'org.projectlombok:lombok:1.18.12'
____________________
**Dockerfile DB**
    
    FROM ubuntu:16.04
    
    # Add the PostgreSQL PGP key to verify their Debian packages.
    # It should be the same key as https://www.postgresql.org/media/keys/ACCC4CF8.asc
    RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8
    
    # Add PostgreSQL's repository. It contains the most recent stable release
    #  of PostgreSQL.
    RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > /etc/apt/sources.list.d/pgdg.list
    
    # Install ``python-software-properties``, ``software-properties-common`` and PostgreSQL 9.3
    #  There are some warnings (in red) that show up during the build. You can hide
    #  them by prefixing each apt-get statement with DEBIAN_FRONTEND=noninteractive
    RUN apt-get update && apt-get install -y python-software-properties software-properties-common postgresql-9.3 postgresql-client-9.3 postgresql-contrib-9.3
    
    # Note: The official Debian and Ubuntu images automatically ``apt-get clean``
    # after each ``apt-get``
    
    # Run the rest of the commands as the ``postgres`` user created by the ``postgres-9.3`` package when it was ``apt-get installed``
    USER postgres
    
    # Create a PostgreSQL role named ``docker`` with ``docker`` as the password and
    # then create a database `docker` owned by the ``docker`` role.
    # Note: here we use ``&&\`` to run commands one after the other - the ``\``
    #       allows the RUN command to span multiple lines.
    RUN    /etc/init.d/postgresql start &&\
        psql --command "CREATE USER docker WITH SUPERUSER PASSWORD 'docker';" &&\
        createdb -O docker docker
    
    # Adjust PostgreSQL configuration so that remote connections to the
    # database are possible.
    RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/9.3/main/pg_hba.conf
    
    # And add ``listen_addresses`` to ``/etc/postgresql/9.3/main/postgresql.conf``
    RUN echo "listen_addresses='*'" >> /etc/postgresql/9.3/main/postgresql.conf
    
    # Expose the PostgreSQL port
    EXPOSE 8082
        
    EXPOSE 9092
    
    # Add VOLUMEs to allow backup of config, logs and databases
    VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]
    
    # Set the default command to run when starting the container
    CMD ["/usr/lib/postgresql/9.3/bin/postgres", "-D", "/var/lib/postgresql/9.3/main", "-c", "config_file=/etc/postgresql/9.3/main/postgresql.conf"]
_________________
**Dockerfile Web**
        
    FROM tomcat
    
    
    RUN apt-get update -y && \ 
        apt-get install iputils-ping -y && \
        apt-get install python3 --yes && \
        apt-get install -y avahi-daemon libnss-mdns && \
        apt-get install -y unzip && \
        apt-get install git -y && \
        apt-get install -y apt-utils && \
        apt-get install nodejs -y && \
        apt-get install npm -y    
    
    RUN mkdir -p /tmp/build
    
    WORKDIR /tmp/build/
    
    RUN git clone https://1191756@bitbucket.org/1191756/devops_g5_alt_survivor.git
    
    WORKDIR /tmp/build/Devops_G5_Alt_Survivor/
    
    RUN chmod u+x ./gradlew
    
    RUN ./gradlew clean build
    
    RUN cp build/libs/training-1.0-SNAPSHOT.jar /usr/local/tomcat/webapps/
    
    EXPOSE 8080
  
***NOTE:***
Unfortunately we didn't have time to fully implement the alternative, we could only complete both Dockerfiles and Docker-compose.yml.
  
  
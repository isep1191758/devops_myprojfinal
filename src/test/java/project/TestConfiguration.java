package project;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import project.exceptions.RestExceptionHandler;
import project.services.*;

@Profile("test")
@Configuration
public class TestConfiguration {
    @Bean
    @Primary
    public AddAccountToGroupService addAccountToGroupMockService() {
        return Mockito.mock(AddAccountToGroupService.class);
    }

    @Bean
    @Primary
    public AddAccountToPersonService addAccountToPersonMockService() {
        return Mockito.mock(AddAccountToPersonService.class);
    }

    @Bean
    @Primary
    public AddCategoryToGroupService AddCategoryToGroupMockService() {
        return Mockito.mock(AddCategoryToGroupService.class);
    }

    @Bean
    @Primary
    public AddMemberToGroupService addMemberToGroupMockService() {
        return Mockito.mock(AddMemberToGroupService.class);
    }

    @Bean
    @Primary
    public GetFamiliesService GetFamiliesMockService() {
        return Mockito.mock(GetFamiliesService.class);
    }

    @Bean
    @Primary
    public IsSiblingPersonService isSiblingPersonMockService() {
        return Mockito.mock(IsSiblingPersonService.class);
    }

    @Bean
    @Primary
    public AddGroupService AddGroupMockService() {
        return Mockito.mock(AddGroupService.class);
    }

    @Bean
    @Primary
    public RestExceptionHandler RestExceptionMockHandler() {
        return Mockito.mock(RestExceptionHandler.class);
    }
}



package project.controllers.cli;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import project.dto.GroupDTO;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;
import project.services.GetFamiliesService;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * US004 - Como gestor quero saber quais os grupos que são família, i.e. grupo constituído por pai, mãe e um sub-conjunto (não vazio,
 * mas podem ser todos) dos respetivos filhos.
 */

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class GetFamiliesControllerTest {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    GetFamiliesController getFamiliesController;
    @Autowired
    GetFamiliesService getFamiliesMockService;

    @DisplayName("getFamilies - Happy Path")
    @Test
    @Transactional
    void getFamiliesHappyPath() {
        //Arrange
        groupRepository.deleteAll();

        Person father;
        Person mother;
        Person son;
        Group familyGroup;
        Group friendsGroup;
        Description familyDescription;
        Description friendsDescription;
        Name maria;
        Name jose;
        Name tarcisio;
        Email mariaEmail;
        Email tarcisioEmail;
        Email joseEmail;
        Address birthAddress;
        Date parentsBirthdate;
        Date personsBirthdate;
        //Families
        birthAddress = new Address("Porto, Portugal");
        parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        maria = new Name("Maria");
        mariaEmail = new Email("mariaaalegria@family.com");
        mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        jose = new Name("José");
        joseEmail = new Email("josebide@family.com");
        father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        tarcisio = new Name("Tarcisio");
        tarcisioEmail = new Email("tarcisiobelo@family.com");
        son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());

        personRepository.save(mother);
        personRepository.save(father);
        personRepository.save(son);

        // GROUPS
        familyDescription = new Description("Good Members Family");
        familyGroup = new Group(familyDescription, father.getPersonID());
        familyGroup.addMember(mother.getPersonID());
        familyGroup.addMember(son.getPersonID());

        groupRepository.save(familyGroup);

        GroupID groupID = new GroupID(new Description("Good Members Family"));
        Set<PersonID> responsibles = new HashSet<>();
        responsibles.add(father.getPersonID());
        Set<PersonID> members = new HashSet<>();
        members.add(father.getPersonID());
        members.add(mother.getPersonID());
        members.add(son.getPersonID());

        Set<Category> categories = new HashSet<>();

        GroupDTO expectedGroupDTO = new GroupDTO();
        expectedGroupDTO.setGroupID(groupID);
        expectedGroupDTO.setResponsibles(responsibles);
        expectedGroupDTO.setMembers(members);
        expectedGroupDTO.setCategories(categories);

        Set<GroupDTO> expectedFamiliesDTOList = new HashSet<>();
        expectedFamiliesDTOList.add(expectedGroupDTO);

        Mockito.when(getFamiliesMockService.getFamilies()).thenReturn(expectedFamiliesDTOList);

        boolean isSame = false;

        //Act
        Set<GroupDTO> result = getFamiliesController.getFamilies();

        for (GroupDTO iterator : result) {
            isSame = iterator.getGroupID().equals(expectedGroupDTO.getGroupID());
        }

        //Assert
        assertTrue(isSame);
    }
}
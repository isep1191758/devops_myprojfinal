package project.controllers.web.integration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.dto.AddAccountToPersonRequestInfoDTO;
import project.model.person.Person;
import project.model.shared.Address;
import project.model.shared.Date;
import project.model.shared.Email;
import project.model.shared.Name;
import project.repositories.PersonRepository;
import project.utils.GetJsonNodeValue;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AddAccountToPersonRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    PersonRepository personRepository;

    @BeforeEach
    void setUpForTests() {
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name albertino = new Name("Albertino");
        Email albertinoEmail = new Email("albertino@family.com");
        Person albertinoPerson = new Person(albertino, birthAddress, personsBirthdate, albertinoEmail, null, null);

        personRepository.save(albertinoPerson);
    }

    @DisplayName("addAccountToPerson - Happy Path")
    @Test
    void addAccountToPersonHappyPath() throws Exception {
        //Arrange
        final String uri = "/persons/albertino@family.com/accounts";

        final String denomination = "Groceries Account";
        final String description = "Everything Juicy";

        AddAccountToPersonRequestInfoDTO accountToPersonRequestInfoDTO = new AddAccountToPersonRequestInfoDTO(denomination, description);

        String entryJson = super.mapToJson(accountToPersonRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).characterEncoding("UTF-8").content(entryJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);

        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("Groceries Account", GetJsonNodeValue.nodeAsString(content, "denomination"));
        assertEquals("Everything Juicy", GetJsonNodeValue.nodeAsString(content, "description"));
        assertEquals("albertino@family.com", GetJsonNodeValue.nodeAsString(content, "ownerID"));
    }

    @DisplayName("addAccountToPerson - Already Exists")
    @Test
    void addAccountToPersonAlreadyExists() throws Exception {
        //Arrange
        final String uri = "/persons/albertino@family.com/accounts";

        final String denomination = "GroceriesShop Account";
        final String description = "Everything is Juicy";

        AddAccountToPersonRequestInfoDTO accountToPersonRequestInfoDTO = new AddAccountToPersonRequestInfoDTO(denomination, description);

        String entryJson = super.mapToJson(accountToPersonRequestInfoDTO);

        mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).characterEncoding("UTF-8").content(entryJson)).andReturn();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).characterEncoding("UTF-8").content(entryJson)).andReturn();

        String content = mvcResult.getResponse().getContentAsString();

        assertEquals("Please select an nonexistent Account", GetJsonNodeValue.nodeAsString(content, "message"));
        assertEquals("UNPROCESSABLE_ENTITY", GetJsonNodeValue.nodeAsString(content, "status"));
    }
}
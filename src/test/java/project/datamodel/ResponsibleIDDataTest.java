package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.shared.Description;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class ResponsibleIDDataTest {
    @Test
    void getId() {
        //Arrange
        ResponsibleIDData noArgsResponsibleIDData = new ResponsibleIDData();
        ResponsibleIDData responsibleIDData = new ResponsibleIDData("John Doe");

        String expected = "John Doe";

        //Act
        String result = responsibleIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setId() {
        //Arrange
        ResponsibleIDData responsibleIDData = new ResponsibleIDData("John Doe");

        String expected = "John Doe 2";

        //Act
        responsibleIDData.setId("John Doe 2");
        String result = responsibleIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testEquals() {
        //Arrange
        ResponsibleIDData responsibleIDData = new ResponsibleIDData("John Doe");

        //Act
        boolean result = responsibleIDData.equals(responsibleIDData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentObjectSameInfo() {
        //Arrange
        ResponsibleIDData responsibleIDData = new ResponsibleIDData("John Doe");
        ResponsibleIDData responsibleIDDataSameInfo = new ResponsibleIDData("John Doe");

        //Act
        //Assert
        assertEquals(responsibleIDDataSameInfo, responsibleIDData);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        ResponsibleIDData responsibleIDData = new ResponsibleIDData("John Doe");
        ResponsibleIDData responsibleIDDataSameInfo = null;

        //Act
        boolean result = responsibleIDData.equals(responsibleIDDataSameInfo);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObject() {
        //Arrange
        ResponsibleIDData responsibleIDData = new ResponsibleIDData("John Doe");
        Description description = new Description("Description");

        //Act
        boolean result = responsibleIDData.equals(description);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        ResponsibleIDData responsibleIDData = new ResponsibleIDData("John Doe");

        int expected = Objects.hash(responsibleIDData.getId());

        //Act
        int result = responsibleIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeFixedValue() {
        //Arrange
        ResponsibleIDData responsibleIDData = new ResponsibleIDData("John Doe");

        int expected = -1367319356;

        //Act
        int result = responsibleIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        ResponsibleIDData responsibleIDData = new ResponsibleIDData("John Doe");

        String expected = "ResponsibleIDData(id=John Doe)";

        //Act
        String result = responsibleIDData.toString();

        //Assert
        assertEquals(expected, result);
    }
}
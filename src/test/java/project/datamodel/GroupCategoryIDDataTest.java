package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.person.Person;
import project.model.shared.Address;
import project.model.shared.Date;
import project.model.shared.Email;
import project.model.shared.Name;

import java.time.LocalDateTime;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class GroupCategoryIDDataTest {
    @Test
    void getId() {
        //Arrange
        GroupCategoryIDData noArgsGroup = new GroupCategoryIDData();
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);

        String expected = "Category Example";

        //Act
        String result = groupCategoryIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setId() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);

        groupCategoryIDData.setId("Category Example Modified");

        String expected = "Category Example Modified";

        //Act
        String result = groupCategoryIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void getGroup() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);

        //Act
        GroupData result = groupCategoryIDData.getGroup();

        //Assert
        assertEquals(groupData, result);
    }

    @Test
    void setGroup() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);

        groupCategoryIDData.setGroup(groupData);

        //Act
        GroupData result = groupCategoryIDData.getGroup();

        //Assert
        assertEquals(groupData, result);
    }

    @Test
    void testEqualsSameDifferentObject() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);

        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);
        GroupCategoryIDData groupCategoryIDDataSameInfo = new GroupCategoryIDData("Category Example", groupData);

        //Act
        boolean result = groupCategoryIDData.equals(groupCategoryIDDataSameInfo);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsSameObject() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);

        //Act
        boolean result = groupCategoryIDData.equals(groupCategoryIDData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);

        GroupCategoryIDData nullGroupCategoryData = null;

        //Act
        boolean result = groupCategoryIDData.equals(nullGroupCategoryData);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObject() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);

        //Act
        boolean result = groupCategoryIDData.equals(groupData);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);

        int expected = Objects.hash(groupCategoryIDData.getId(), groupCategoryIDData.getGroup());

        //Act
        int result = groupCategoryIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeFixedValue() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);

        int expected = -31151576;

        //Act
        int result = groupCategoryIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person person = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        GroupData groupData = new GroupData(new GroupIDData("Test Group"), new Date(LocalDateTime.now()).toString());
        GroupCategoryData categoryDataExample = new GroupCategoryData("Test category", groupData);
        groupData.setResponsible(new ResponsibleIDData(person.getPersonID()));
        groupData.setMember(new MemberIDData(person.getPersonID()));

        GroupCategoryIDData groupCategoryIDData = new GroupCategoryIDData("Category Example", groupData);

        String expected = "GroupCategoryIDData(id=Category Example, group=GroupData{id=GroupIDData(id=Test Group), responsibles=[ResponsibleIDData(id=maria@family.com)], members=[MemberIDData(id=maria@family.com)], categories=[]})";

        //Act
        String result = groupCategoryIDData.toString();

        //Assert
        assertEquals(expected, result);
    }
}
package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

class TransactionDTOTest {
    TransactionDTO transactionDTO;

    Person mario;
    Email marioEmail;
    Address marioBirthPlace;
    Date marioBirthDate;
    Group group;
    PersonID personID;
    PersonID personID2;
    GroupID groupID;
    GroupID groupID2;
    double amount;
    double amount2;
    Description description;
    Description description2;
    Category category;
    Category category2;
    AccountID debit;
    AccountID debit2;
    AccountID credit;
    AccountID credit2;
    TransactionType type;
    TransactionType type2;
    TransactionDate date;
    TransactionDate date2;

    @BeforeEach
    void setUp() {
        // Arrange
        marioEmail = new Email("mario@family.com");
        marioBirthPlace = new Address("Porto");
        marioBirthDate = new Date(LocalDateTime.of(1990, Month.JANUARY, 1, 1, 1, 1));
        mario = new Person(new Name("Mario"), marioBirthPlace, marioBirthDate, marioEmail, null, null);
        personID = mario.getPersonID();
        personID2 = new PersonID(new Email("mario.family@family.com"));
        group = new Group(new Description("Family"), mario.getPersonID());
        groupID = group.getID();
        groupID2 = new GroupID(new Description("Work"));
        amount = 20.0;
        amount2 = 35.0;
        description = new Description("Lunch");
        description2 = new Description("tool");
        category = new Category(new Designation("Funny time"));
        category2 = new Category(new Designation("Pocket"));
        debit = new AccountID(new Denomination("Pocket"), groupID);
        debit2 = new AccountID(new Denomination("wallet"), groupID);
        credit = new AccountID(new Denomination("Restaurante Pinheiro"), groupID);
        credit2 = new AccountID(new Denomination("market"), groupID);
        type = TransactionType.DEBIT;
        type2 = TransactionType.CREDIT;
        date = new TransactionDate(LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0, 0));
        date2 = new TransactionDate(LocalDateTime.of(2020, Month.FEBRUARY, 1, 0, 0, 0));

        transactionDTO = new TransactionDTO(amount, description, date, category, debit, credit, type);
    }

    @DisplayName("Get Amount")
    @Test
    void getAmount() {
        double result = transactionDTO.getAmount();
    }

    @DisplayName("Set Amount")
    @Test
    void setAmount() {
        transactionDTO.setAmount(amount2);
    }

    @DisplayName("Get Description")
    @Test
    void getDescription() {
        Description result = transactionDTO.getDescription();
    }

    @DisplayName("Set Description")
    @Test
    void setDescription() {
        transactionDTO.setDescription(description2);
    }

    @DisplayName("Get Category")
    @Test
    void getCategory() {
        Category resut = transactionDTO.getCategory();
    }

    @DisplayName("Set Category")
    @Test
    void setCategory() {
        transactionDTO.setCategory(category2);
    }

    @DisplayName("Get Debit")
    @Test
    void getDebit() {
        AccountID result = transactionDTO.getDebit();
    }

    @DisplayName("Set Debit")
    @Test
    void setDebit() {
        transactionDTO.setDebit(debit2);
    }

    @DisplayName("Get Credit")
    @Test
    void getCredit() {
        AccountID result = transactionDTO.getCredit();
    }

    @DisplayName("Set Credit")
    @Test
    void setCredit() {
        transactionDTO.setCredit(credit2);
    }

    @DisplayName("Get Type")
    @Test
    void getType() {
        TransactionType result = transactionDTO.getType();
    }

    @DisplayName("Set type")
    @Test
    void setType() {
        transactionDTO.setType(type2);
    }


    @Test
    void getDate() {
        TransactionDate result = transactionDTO.getDate();
    }

    @Test
    void setDate() {
        transactionDTO.setDate(date2);
    }
}
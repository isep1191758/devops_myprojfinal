package project.dto.assemblers;

import org.junit.jupiter.api.Test;
import project.dto.IsSiblingRequestDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class IsSiblingRequestDTOAssemblerTest {

    @Test
    void mapToDTO() {
        //Arrange
        String aPersonEmail = "duarte@stcp.com";
        String aSecondPersonEmail = "marta@stcp.com";

        IsSiblingRequestDTO isSiblingRequestDTO = new IsSiblingRequestDTO(aPersonEmail, aSecondPersonEmail);

        //Act
        IsSiblingRequestDTO result = IsSiblingRequestDTOAssembler.mapToDTO(aPersonEmail, aSecondPersonEmail);

        //Assert
        assertEquals(isSiblingRequestDTO, result);
    }

    @Test
    void IsSiblingRequestDTOAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            IsSiblingRequestDTOAssembler isSiblingRequestDTOAssembler = new IsSiblingRequestDTOAssembler();
        });
    }
}
package project.dto.assemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.CategoryDTO;
import project.model.shared.Category;
import project.model.shared.Designation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CategoryAssemblerTest {

    @DisplayName("CategoryAssembler - Happy Case")
    @Test
    void CategoryAssemblerHappyCase() {
        //Arrange
        Designation designation = new Designation("Tennis");
        Category category1 = new Category(designation);
        Category category2 = new Category(designation);
        //Act
        CategoryDTO categoryDTO1 = CategoryAssembler.mapToDTO(category1);
        CategoryDTO categoryDTO2 = CategoryAssembler.mapToDTO(category2);
        //Assert
        assertEquals(categoryDTO1.getDesignation(), categoryDTO2.getDesignation());
    }

    @Test
    void CategoryAssemblerError() {
        assertThrows(AssertionError.class, CategoryAssembler::new);
    }
}
package project.dto.assemblers;

import org.junit.jupiter.api.Test;
import project.dto.PersonDTO;
import project.model.person.Person;
import project.model.shared.Address;
import project.model.shared.Date;
import project.model.shared.Email;
import project.model.shared.Name;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PersonAssemblerTest {
    @Test
    void mapToDTO() {
        Address birthAddress = new Address("VNGaia, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1980, 9, 11, 1, 2));
        Name duarte = new Name("Duarte");
        Email duarteEmail = new Email("duarte@stcp.com");
        Person son = new Person(duarte, birthAddress, personsBirthdate, duarteEmail, null, null);

        PersonDTO expected = new PersonDTO();
        expected.setPersonID(son.getPersonID());
        expected.setName(son.getName());
        expected.setBirthPlace(son.getBirthPlace());
        expected.setBirthDate(son.getBirthDate());
        expected.setMother(son.getMother());
        expected.setFather(son.getFather());
        expected.setSiblings(son.getSiblings());
        expected.setCategories(son.getCategories());

        PersonDTO result = PersonAssembler.mapToDTO(son);

        assertEquals(expected, result);
    }

    @Test
    void mapToDTOName() {
        Address birthAddress = new Address("VNGaia, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1980, 9, 11, 1, 2));
        Name duarte = new Name("Duarte");
        Email duarteEmail = new Email("duarte@stcp.com");
        Person son = new Person(duarte, birthAddress, personsBirthdate, duarteEmail, null, null);

        PersonDTO expected = new PersonDTO();
        expected.setPersonID(son.getPersonID());
        expected.setName(son.getName());
        expected.setBirthPlace(son.getBirthPlace());
        expected.setBirthDate(son.getBirthDate());
        expected.setMother(son.getMother());
        expected.setFather(son.getFather());
        expected.setSiblings(son.getSiblings());
        expected.setCategories(son.getCategories());

        PersonDTO result = PersonAssembler.mapToDTO(son);

        assertEquals("Duarte", result.getName().toString());
    }

    @Test
    void mapToDTOBirthPlace() {
        Address birthAddress = new Address("VNGaia, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1980, 9, 11, 1, 2));
        Name duarte = new Name("Duarte");
        Email duarteEmail = new Email("duarte@stcp.com");
        Person son = new Person(duarte, birthAddress, personsBirthdate, duarteEmail, null, null);

        PersonDTO expected = new PersonDTO();
        expected.setPersonID(son.getPersonID());
        expected.setName(son.getName());
        expected.setBirthPlace(son.getBirthPlace());
        expected.setBirthDate(son.getBirthDate());
        expected.setMother(son.getMother());
        expected.setFather(son.getFather());
        expected.setSiblings(son.getSiblings());
        expected.setCategories(son.getCategories());

        PersonDTO result = PersonAssembler.mapToDTO(son);

        assertEquals("VNGaia, Portugal", result.getBirthPlace().toString());
    }

    @Test
    void mapToDTOBirthDate() {
        Address birthAddress = new Address("VNGaia, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1980, 9, 11, 1, 2));
        Name duarte = new Name("Duarte");
        Email duarteEmail = new Email("duarte@stcp.com");
        Person son = new Person(duarte, birthAddress, personsBirthdate, duarteEmail, null, null);

        PersonDTO expected = new PersonDTO();
        expected.setPersonID(son.getPersonID());
        expected.setName(son.getName());
        expected.setBirthPlace(son.getBirthPlace());
        expected.setBirthDate(son.getBirthDate());
        expected.setMother(son.getMother());
        expected.setFather(son.getFather());
        expected.setSiblings(son.getSiblings());
        expected.setCategories(son.getCategories());
        PersonDTO result = PersonAssembler.mapToDTO(son);

        assertEquals("1980-09-11", result.getBirthDate().toString());
    }

    @Test
    void mapToDTOMother() {
        Address birthAddress = new Address("VNGaia, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1980, 9, 11, 1, 2));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person mother = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        Person father = new Person(jose, birthAddress, personsBirthdate, joseEmail, null, null);

        Name duarte = new Name("Duarte");
        Email duarteEmail = new Email("duarte@stcp.com");
        Person son = new Person(duarte, birthAddress, personsBirthdate, duarteEmail, mother.getPersonID(), father.getPersonID());

        PersonDTO expected = new PersonDTO();
        expected.setPersonID(son.getPersonID());
        expected.setName(son.getName());
        expected.setBirthPlace(son.getBirthPlace());
        expected.setBirthDate(son.getBirthDate());
        expected.setMother(son.getMother());
        expected.setFather(son.getFather());
        expected.setSiblings(son.getSiblings());
        expected.setCategories(son.getCategories());
        PersonDTO result = PersonAssembler.mapToDTO(son);

        assertEquals("maria@family.com", result.getMother().getEmail().getEmail());
    }

    @Test
    void mapToDTOFather() {
        Address birthAddress = new Address("VNGaia, Portugal");
        Date personsBirthdate = new Date(LocalDateTime.of(1980, 9, 11, 1, 2));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("maria@family.com");
        Person mother = new Person(maria, birthAddress, personsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("jose@family.com");
        Person father = new Person(jose, birthAddress, personsBirthdate, joseEmail, null, null);

        Name duarte = new Name("Duarte");
        Email duarteEmail = new Email("duarte@stcp.com");
        Person son = new Person(duarte, birthAddress, personsBirthdate, duarteEmail, mother.getPersonID(), father.getPersonID());

        PersonDTO expected = new PersonDTO();
        expected.setPersonID(son.getPersonID());
        expected.setName(son.getName());
        expected.setBirthPlace(son.getBirthPlace());
        expected.setBirthDate(son.getBirthDate());
        expected.setMother(son.getMother());
        expected.setFather(son.getFather());
        expected.setSiblings(son.getSiblings());
        expected.setCategories(son.getCategories());
        PersonDTO result = PersonAssembler.mapToDTO(son);

        assertEquals("jose@family.com", result.getFather().getEmail().getEmail());
    }

    @Test
    void PersonAssemblerError() throws Exception {
        assertThrows(AssertionError.class, () -> {
            PersonAssembler personAssembler = new PersonAssembler();
        });
    }
}
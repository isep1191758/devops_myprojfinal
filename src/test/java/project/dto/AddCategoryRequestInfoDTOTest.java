package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.Name;

import static org.junit.jupiter.api.Assertions.*;

public class AddCategoryRequestInfoDTOTest {

    String designation;
    String groupDescription;
    String responsibleEmail;
    AddCategoryRequestInfoDTO addCategoryRequestInfoDTO;

    @BeforeEach
    void setUp() {
        designation = "equipment";
        groupDescription = "Soccer club";
        responsibleEmail = "Rui Frederico";
        addCategoryRequestInfoDTO = new AddCategoryRequestInfoDTO(designation, groupDescription, responsibleEmail);
    }

    @Test
    void getDesignationTest() {
        addCategoryRequestInfoDTO.getDesignation();
    }

    @Test
    void setDesignationTest() {
        addCategoryRequestInfoDTO.setDesignation(designation);
    }

    @Test
    void getGroupDescriptionTest() {
        addCategoryRequestInfoDTO.getGroupDescription();
    }

    @Test
    void setGroupDescriptionTest() {
        addCategoryRequestInfoDTO.setGroupDescription(groupDescription);
    }

    @Test
    void getPersonIDTest() {
        addCategoryRequestInfoDTO.getGroupDescription();
    }

    @Test
    void setPersonIDTest() {
        addCategoryRequestInfoDTO.setPersonID(responsibleEmail);
    }

    @DisplayName("AddCategoryRequestInfoDTOEqualsTrue - Testing Equals")
    @Test
    void AddCategoryRequestInfoDTOEqualsTrue() {
        //Act
        AddCategoryRequestInfoDTO expected = new AddCategoryRequestInfoDTO(designation, groupDescription, responsibleEmail);
        AddCategoryRequestInfoDTO result = addCategoryRequestInfoDTO;

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("AddCategoryRequestInfoDTOEqualsTrue - Same Exact Object")
    @Test
    void AddCategoryRequestInfoDTOSameExactObject() {
        //Act
        AddCategoryRequestInfoDTO result = addCategoryRequestInfoDTO;

        //Assert
        assertEquals(result, result);
    }

    @DisplayName("AddCategoryRequestInfoDTOEqualsTrue - Different Objects")
    @Test
    void AddCategoryRequestInfoDTODiffObjects() {
        //Act
        String designationDif = "Trips";
        String groupDescriptionDiff = "Cheerleaders";
        String responsibleEmailDiff = "tito@mail.com";
        AddCategoryRequestInfoDTO expected = new AddCategoryRequestInfoDTO(designationDif, groupDescriptionDiff, responsibleEmailDiff);
        AddCategoryRequestInfoDTO result = addCategoryRequestInfoDTO;

        //Assert
        assertNotEquals(expected, result);
    }

    @DisplayName("AddCategoryRequestInfoDTOEqualsTrue - Different type of Object")
    @Test
    void AddCategoryRequestInfoDTODifferentTypeOFObject() {
        Name name = new Name("Name");

        //Act
        AddCategoryRequestInfoDTO result = addCategoryRequestInfoDTO;

        //Assert
        assertNotEquals(name, result);
    }

    @DisplayName("AddCategoryRequestInfoDTOEqualsTrue - Null Object")
    @Test
    void AddCategoryRequestInfoDTONullObject() {
        //Act
        AddCategoryRequestInfoDTO result = addCategoryRequestInfoDTO;

        //Assert
        assertFalse(result.equals(null));
    }

    @DisplayName("Equals not same designation")
    @Test
    void AddCategoryRequestInfoDTOEqualsFalseDesignation() {
        //Act
        designation = "Ball";
        AddCategoryRequestInfoDTO expected = new AddCategoryRequestInfoDTO(designation, groupDescription, responsibleEmail);
        AddCategoryRequestInfoDTO result = addCategoryRequestInfoDTO;

        //Assert
        assertNotEquals(expected, result);
    }

    @DisplayName("Equals not same groupDescription")
    @Test
    void AddCategoryRequestInfoDTOEqualsFalseGroupDesc() {
        //Act
        groupDescription = "Dancers";
        AddCategoryRequestInfoDTO expected = new AddCategoryRequestInfoDTO(designation, groupDescription, responsibleEmail);
        AddCategoryRequestInfoDTO result = addCategoryRequestInfoDTO;

        //Assert
        assertNotEquals(expected, result);
    }

    @DisplayName("Equals not same responsibleEmail")
    @Test
    void AddCategoryRequestInfoDTOEqualsFalseEmail() {
        //Act
        responsibleEmail = "lala@mail.com";
        AddCategoryRequestInfoDTO expected = new AddCategoryRequestInfoDTO(designation, groupDescription, responsibleEmail);
        AddCategoryRequestInfoDTO result = addCategoryRequestInfoDTO;

        //Assert
        assertNotEquals(expected, result);
    }

    @DisplayName("hashCode() - Happy Path")
    @Test
    void hashCodeHappyPath() {
        int expected = 1516386563;
        int result = addCategoryRequestInfoDTO.hashCode();

        assertEquals(expected, result);
    }
}
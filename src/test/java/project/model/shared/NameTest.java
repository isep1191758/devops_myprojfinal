package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.EmptyException;

import static org.junit.jupiter.api.Assertions.*;


class NameTest {

    @DisplayName("create name - Happy Path")
    @Test
    void setIsNameValidHappyPath() {
        //Arrange//Act//Assert
        new Name("Joana");
    }

    @DisplayName("create name - Null")
    @Test
    void setIsNameValidNull() {
        //Arrange//Act//Assert
        assertThrows(NullPointerException.class, () -> {
            new Name(null);
        });
    }

    @DisplayName("create name - Empty")
    @Test
    void setIsNameValidEmpety() {
        //Arrange//Act//Assert
        assertThrows(EmptyException.class, () -> {
            new Name("");
        });
    }

    @DisplayName("equals() - Same Object")
    @Test
    void equalsSameObject() {
        //Arrange
        Name nameOne = new Name("Joana");

        //Act
        boolean result = nameOne.equals(nameOne);

        //Assert
        assertTrue(result);
    }

    @DisplayName("equals() - Object from different class")
    @Test
    void equalsDifferentClassObject() {
        //Arrange
        Name nameOne = new Name("Joana");
        Description description = new Description("Descrição");

        //Act
        boolean result = nameOne.equals(description);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create name - OverrideEquals")
    @Test
    void setIsNameValidOverrideEquals() {
        //Arrange
        Name nameOne = new Name("Joana");
        Name nameTwo = new Name("Joana");

        boolean expected = true;

        //Act
        boolean result = nameOne.equals(nameTwo);

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("create name - OverrideEqualsFalse")
    @Test
    void setIsNameValidOverrideEqualsFalse() {
        //Arrange
        Name nameOne = new Name("Joana");
        Name nameTwo = new Name("Joaquina");

        boolean expected = false;

        //Act
        boolean result = nameOne.equals(nameTwo);

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("create name - OverrideEqualsNull")
    @Test
    void setIsNameValidOverrideEqualsNull() {
        //Arrange
        Name nameOne = new Name("Joana");

        boolean expected = false;

        //Act
        boolean result = nameOne.equals(null);

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("hashCode() - Happy Path")
    @Test
    void hashCodeHappyPath() {
        //Arrange
        Name nameOne = new Name("Joana");

        //Act
        int result = nameOne.hashCode();

        //Assert
        assertEquals(71744110, result);
    }
}


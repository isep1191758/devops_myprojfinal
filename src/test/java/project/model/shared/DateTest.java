package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class DateTest {
    @DisplayName("create date - Happy Path")
    @Test
    void setIsAddressValidHappyPath() {
        //Arrange//Act//Assert
        LocalDateTime.of(1999, 03, 10, 22, 10, 30);
    }

    @DisplayName("create date - sameValueAs")
    @Test
    void setIsDateValidSameValueAs() {
        //Arrange
        Date dateOne = new Date(LocalDateTime.of(1999, 03, 10, 22, 10, 30));
        Date dateTwo = new Date(LocalDateTime.of(1999, 03, 10, 22, 10, 30));

        //Act
        boolean result = dateOne.sameValueAs(dateTwo);

        //Assert
        assertTrue(result);
    }

    @DisplayName("create date - sameValueAsFalse")
    @Test
    void setIsDateValidSameValueAsFalse() {
        //Arrange
        Date dateOne = new Date(LocalDateTime.of(1999, 03, 10, 22, 10, 30));
        Date dateTwo = new Date(LocalDateTime.of(1988, 03, 10, 22, 10, 30));

        //Act
        boolean result = dateOne.sameValueAs(dateTwo);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create date - sameValueAsNull")
    @Test
    void setIsDateValidSameValueAsNull() {
        //Arrange
        Date dateOne = new Date(LocalDateTime.of(1999, 03, 10, 22, 10, 30));

        //Act
        boolean result = dateOne.sameValueAs(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create date - Override Equals False")
    @Test
    void setIsDateValidOverrideEqualsFalse() {
        //Arrange
        Date dateOne = new Date(LocalDateTime.of(1988, 03, 10, 22, 10, 30));
        Date dateTwo = new Date(LocalDateTime.of(1888, 03, 10, 22, 10, 30));

        //Act
        boolean result = dateOne.equals(dateTwo);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create date - Override Equals Null")
    @Test
    void setIsDateValidOverrideEqualsNull() {
        //Arrange
        Date dateOne = new Date(LocalDateTime.of(1988, 03, 10, 22, 10, 30));

        //Act
        boolean result = dateOne.equals(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("Create date - Null LocalDateTime")
    @Test
    void setIsDateValidNullLocalDateTime() {
        //Arrange
        LocalDateTime dateTest = null;

        //Act
        assertThrows(NullPointerException.class, () -> {
            Date date = new Date(dateTest);
        });
    }

    @DisplayName("create date - Override Equals Same Object")
    @Test
    void setIsDateValidOverrideEqualsSameObject() {
        //Arrange
        Date dateOne = new Date(LocalDateTime.of(1988, 03, 10, 22, 10, 30));

        //Act
        boolean result = dateOne.equals(dateOne);

        //Assert
        assertTrue(result);
    }

    @DisplayName("create date - Override Equals Different Class")
    @Test
    void setIsDateValidOverrideEqualsDifferentClass() {
        //Arrange
        Date dateOne = new Date(LocalDateTime.of(1988, 03, 10, 22, 10, 30));
        Name name = new Name("Ludovina");
        //Act
        boolean result = dateOne.equals(name);

        //Assert
        assertFalse(result);
    }
}
package project.datamodel;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor

@Entity
@Table(name = "groups")
public class GroupData {
    @Id
    private GroupIDData id;
    private String creationDate;
    @ElementCollection
    @CollectionTable(name = "groups_responsibles", joinColumns = @JoinColumn(name = "group_id"))
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<ResponsibleIDData> responsibles;
    @ElementCollection
    @CollectionTable(name = "groups_members", joinColumns = @JoinColumn(name = "group_id"))
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<MemberIDData> members;
    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
    private List<GroupCategoryData> categories;

    public GroupData(GroupIDData id, String creationDate) {
        this.id = id;
        this.creationDate = creationDate;
        this.responsibles = new ArrayList<>();
        this.members = new ArrayList<>();
        this.categories = new ArrayList<>();
    }

    public GroupIDData getId() {
        return id;
    }

    public void setId(GroupIDData id) {
        this.id = id;
    }

    public List<ResponsibleIDData> getResponsibles() {
        return responsibles;
    }

    public void setResponsibles(List<ResponsibleIDData> responsibles) {
        this.responsibles = responsibles;
    }

    public void setResponsible(ResponsibleIDData responsible) {
        this.responsibles.add(responsible);
    }

    public List<MemberIDData> getMembers() {
        return members;
    }

    public void setMembers(List<MemberIDData> members) {
        this.members = members;
    }

    public List<GroupCategoryData> getCategories() {
        return categories;
    }

    public void setCategories(List<GroupCategoryData> categories) {
        this.categories = categories;
    }

    public void setMember(MemberIDData member) {
        this.members.add(member);
    }

    public void setCategory(GroupCategoryData category) {
        this.categories.add(category);
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GroupData)) {
            return false;
        }
        GroupData groupData = (GroupData) o;
        return Objects.equals(id, groupData.id);
    }

    @Override
    public String toString() {
        return "GroupData{" +
                "id=" + id +
                ", responsibles=" + responsibles +
                ", members=" + members +
                ", categories=" + categories +
                '}';
    }
}
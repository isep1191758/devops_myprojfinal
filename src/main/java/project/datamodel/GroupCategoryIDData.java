package project.datamodel;

import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@ToString

public class GroupCategoryIDData implements Serializable {
    private static final long serialVersionUID = 9198900360351045241L;
    private String id;
    private GroupData group;

    public GroupCategoryIDData(String id, GroupData group) {
        this.id = id;
        this.group = group;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public GroupData getGroup() {
        return group;
    }

    public void setGroup(GroupData group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupCategoryIDData that = (GroupCategoryIDData) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(group, that.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, group);
    }
}


package project.controllers.cli;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.dto.GroupDTO;
import project.services.GetFamiliesService;

import java.util.Set;

@Component
public class GetFamiliesController {
    @Autowired
    GetFamiliesService srv;

    /**
     * @return a collection of GroupDTO that are families (father, mother and at least a son)
     */
    public Set<GroupDTO> getFamilies() {
        return srv.getFamilies();
    }
}

package project.controllers.cli;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.dto.AccountDTO;
import project.dto.AddAccountToPersonRequestDTO;
import project.dto.assemblers.AddAccountToPersonRequestAssembler;
import project.services.AddAccountToPersonService;

@Component
public class AddAccountToPersonController {
    @Autowired
    private AddAccountToPersonService service;

    /**
     * Add an account, attributing a denomination and description, to later be used in my movements
     *
     * @param denomination desc
     * @param description desc
     * @param personEmail desc
     * @return AccountDTO
     */
    public AccountDTO addAccountToPerson(String denomination, String description, String personEmail) {
        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = AddAccountToPersonRequestAssembler.addAccountMapToDTO(denomination, description, personEmail);

        return service.addAccountToPerson(addAccountToPersonRequestDTO);
    }

}

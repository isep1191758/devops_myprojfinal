package project.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.datamodel.LedgerData;
import project.datamodel.assemblers.LedgerDomainDataAssembler;
import project.model.framework.Repository;
import project.model.ledger.Ledger;
import project.model.shared.LedgerID;
import project.repositories.jpa.LedgerJPARepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class LedgerRepository implements Repository {
    @Autowired
    private LedgerJPARepository ledgerJPARepository;

    /**
     * Saves an entity either by creating it or updating it in the persistence
     * store. The reference is no
     * longer valid and use the returned object instead. e.g.,
     *
     * @param entity description
     * @return the object reference to the persisted object.
     */
    public Ledger save(Object entity) {
        Ledger newLedger = (Ledger) entity;
        LedgerData ledgerData = LedgerDomainDataAssembler.toData(newLedger);
        LedgerData savedLedger = ledgerJPARepository.save(ledgerData);
        return LedgerDomainDataAssembler.toDomain(savedLedger);
    }

    /**
     * Gets all entities from the repository.
     *
     * @return all entities from the repository
     */
    public Iterable<Ledger> findAll() {
        Iterable<LedgerData> allLedgerData = ledgerJPARepository.findAll();
        List<Ledger> domainLedgers = new ArrayList<>();
        for (LedgerData LedgerData : allLedgerData) {
            domainLedgers.add(LedgerDomainDataAssembler.toDomain(LedgerData));
        }
        return domainLedgers;
    }

    /**
     * Gets the entity with the specified primary key.
     *
     * @param id description
     * @return the entity with the specified primary key
     */
    public Optional<Ledger> findById(Object id) {
        LedgerID ledgerIdToSearch = (LedgerID) id;
        Optional<Ledger> foundLedger = Optional.empty();

        if (ledgerJPARepository.findById(ledgerIdToSearch).isPresent()) {
            LedgerData LedgerData = ledgerJPARepository.findById(ledgerIdToSearch).get();
            foundLedger = Optional.of(LedgerDomainDataAssembler.toDomain(LedgerData));
        }

        return foundLedger;
    }

    public void deleteAll() {
        ledgerJPARepository.deleteAll();
    }
}

package project.repositories.jpa;

import org.springframework.data.repository.CrudRepository;
import project.datamodel.PersonData;
import project.model.shared.PersonID;

public interface PersonJPARepository extends CrudRepository<PersonData, PersonID> {
}

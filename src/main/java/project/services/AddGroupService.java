package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.datamodel.assemblers.GroupDomainDataAssembler;
import project.dto.AddGroupRequestDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.GroupAssembler;
import project.exceptions.AlreadyExistsException;
import project.model.group.Group;
import project.model.shared.GroupID;
import project.repositories.GroupRepository;

@Service
public class AddGroupService {

    @Autowired
    GroupRepository groupRepository;
    @Autowired
    GroupDomainDataAssembler domainAssembler;

    public AddGroupService(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    /**
     * As a user, I want to create a group, becoming a group administrator.
     *
     * @param dto description
     * @return create a group and add it to the GroupRepository and return a DTO
     */
    public GroupDTO addGroup(AddGroupRequestDTO dto) {
        if (groupRepository.findById(new GroupID(dto.getGroupDescription())).isPresent()) {
            throw new AlreadyExistsException();
        }

        Group newGroup = new Group(dto.getGroupDescription(), dto.getPersonID());

        Group savedGroup = groupRepository.save(newGroup);

        return GroupAssembler.mapToDTO(savedGroup);
    }
}
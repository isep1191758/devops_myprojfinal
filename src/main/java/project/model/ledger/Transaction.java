package project.model.ledger;

import project.model.shared.*;

import java.util.Objects;

public class Transaction {
    private final double amount;
    private final Description description;
    private final TransactionDate date;
    private final Category category;
    private final AccountID debit;
    private final AccountID credit;
    private final TransactionType type;

    public double getAmount() {
        return amount;
    }

    public Transaction(Double amount, Description description, TransactionDate date, Category category, AccountID debit, AccountID credit, TransactionType type) {
        this.amount = amount;
        this.description = description;
        this.date = date;
        this.category = category;
        this.debit = debit;
        this.credit = credit;
        this.type = type;

        if (amount <= 0) {
            throw new IllegalArgumentException("Amount has to be positive!");
        }
        if (description == null || date == null || category == null || debit == null || credit == null) {
            throw new NullPointerException("Check if the fields are not null");
        }
        if (debit == credit) {
            throw new IllegalArgumentException("The debit Account is the same as the credit one!");
        }
        if (type == null) {
            throw new NullPointerException("There is no such transaction type");
        }
    }

    public Category getCategory() {
        return category;
    }

    public AccountID getDebit() {
        return debit;
    }

    public AccountID getCredit() {
        return credit;
    }

    public TransactionType getType() {
        return type;
    }

    public Description getDescription() {
        return description;
    }

    /**
     * <p> Get transaction's date </p>
     *
     * @return copy aDate of this.date
     */
    public TransactionDate getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Double.compare(that.amount, amount) == 0 &&
                Objects.equals(description, that.description) &&
                Objects.equals(date, that.date) &&
                Objects.equals(category, that.category) &&
                Objects.equals(debit, that.debit) &&
                Objects.equals(credit, that.credit) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, description, date, category, debit, credit, type);
    }
}


package project.model.framework;

public interface Root<T> {

    /**
     * Roots compare by identity
     *
     * @param other param description
     * @return boolean
     */
    boolean sameIdentityAs(T other);
}

package project.exceptions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NotFoundException extends RuntimeException {
    static final long serialVersionUID = 1434445341231232343L;

    boolean messageIsDefault;

    public NotFoundException() {
        super();
        messageIsDefault = true;
    }

    public NotFoundException(String errorMessage) {
        super(errorMessage);
        messageIsDefault = false;
    }

    /**
     * Returns last word on method name if thrown error has no Parameter. Else returns default message;
     *
     * @return String
     */
    public String getLastWordOnMethod() {
        String fullCallerClass = this.getStackTrace()[0].getMethodName();

        String pattern = ("[A-Z][a-z]*$");
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(fullCallerClass);

        String message;
        if (this.messageIsDefault) {
            if (m.find()) {
                message = m.group(0);
            } else {
                message = "Element";
            }
        } else {
            message = this.getMessage();
        }
        return message;
    }
}

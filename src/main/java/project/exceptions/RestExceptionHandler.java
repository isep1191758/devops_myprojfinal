package project.exceptions;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<Object> handleNotFound(NotFoundException exception) {
        String message = "Please select an existing " + exception.getLastWordOnMethod();
        String errorMessage = exception.getLastWordOnMethod() + " does not exist";

        ExceptionMessage apiExceptionMessage = new ExceptionMessage(HttpStatus.UNPROCESSABLE_ENTITY, message, Arrays.asList(errorMessage));

        return new ResponseEntity<>(apiExceptionMessage, new HttpHeaders(), apiExceptionMessage.getStatus());
    }

    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity<Object> handleNullPointerException(NullPointerException ex) {
        String fullCallerClass = ex.getStackTrace()[1].getClassName();
        String callerClass = fullCallerClass.substring(fullCallerClass.lastIndexOf(".") + 1);
        String error = "Please verify that " + callerClass + " has a value";

        ExceptionMessage apiExceptionMessage = new ExceptionMessage(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), Arrays.asList(error));

        return new ResponseEntity<>(apiExceptionMessage, new HttpHeaders(), apiExceptionMessage.getStatus());
    }

    @ExceptionHandler({EmptyException.class})
    public ResponseEntity<Object> handleEmptyException(EmptyException ex) {
        String fullCallerClass = ex.getStackTrace()[1].getClassName();
        String callerClass = fullCallerClass.substring(fullCallerClass.lastIndexOf(".") + 1);
        String error = "Please verify that " + callerClass + " is not empty";

        ExceptionMessage apiExceptionMessage = new ExceptionMessage(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), Arrays.asList(error));

        return new ResponseEntity<>(apiExceptionMessage, new HttpHeaders(), apiExceptionMessage.getStatus());
    }

    @ExceptionHandler({AlreadyExistsException.class})
    public ResponseEntity<Object> handleAlreadyExistsException(AlreadyExistsException exception) {
        String message = "Please select an nonexistent " + exception.getLastWordOnMethod();
        String errorMessage = exception.getLastWordOnMethod() + " already exists";

        ExceptionMessage apiExceptionMessage = new ExceptionMessage(HttpStatus.UNPROCESSABLE_ENTITY, message, Arrays.asList(errorMessage));

        return new ResponseEntity<>(apiExceptionMessage, new HttpHeaders(), apiExceptionMessage.getStatus());
    }
}

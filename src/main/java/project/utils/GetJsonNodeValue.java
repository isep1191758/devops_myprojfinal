package project.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GetJsonNodeValue {
    /**
     * Method tha returns the value of a specific json node
     *
     * @param content param description
     * @param nodeName param description
     * @return string that is the node value
     * @throws JsonProcessingException  param description
     */
    public static String nodeAsString(String content, String nodeName) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(content);
        JsonNode returnNode = rootNode.path(nodeName);

        return returnNode.asText();
    }
}

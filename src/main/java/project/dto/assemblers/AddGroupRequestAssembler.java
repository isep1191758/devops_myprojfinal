package project.dto.assemblers;

import project.dto.AddGroupRequestDTO;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

public class AddGroupRequestAssembler {

    protected AddGroupRequestAssembler() {
        throw new AssertionError();
    }

    public static AddGroupRequestDTO mapToDTO(String groupDescription, String responsibleEmail) {
        Description description = new Description(groupDescription);
        PersonID personID = new PersonID(new Email(responsibleEmail));

        AddGroupRequestDTO resultDTO = new AddGroupRequestDTO(description, personID);

        return resultDTO;
    }
}

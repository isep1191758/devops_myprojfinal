package project.dto.assemblers;

import project.dto.AddCategoryRequestDTO;
import project.model.shared.*;

public class AddCategoryRequestAssembler {
    protected AddCategoryRequestAssembler() {
        throw new AssertionError();
    }

    /**
     * This will mapTo a RequestDTO with information from web and transforms it within CLI
     *
     * @param designation      String parameter from JSON
     * @param groupDescription String parameter from JSON
     * @param responsibleEmail String parameter from JSON
     * @return AddCategoryEntryDTO(adesignation, gpID, pID)
     */
    public static AddCategoryRequestDTO addCategoryMapToDTO(String designation, String groupDescription, String responsibleEmail) {
        Designation adesignation = new Designation(designation);
        GroupID gpID = new GroupID(new Description(groupDescription));
        PersonID pID = new PersonID(new Email(responsibleEmail));

        return new AddCategoryRequestDTO(adesignation, gpID, pID);
    }
}
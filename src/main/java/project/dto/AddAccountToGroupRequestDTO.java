package project.dto;

import project.model.shared.Denomination;
import project.model.shared.Description;
import project.model.shared.GroupID;
import project.model.shared.PersonID;

import java.util.Objects;

public class AddAccountToGroupRequestDTO {
    private Denomination denomination;
    private Description description;
    private PersonID personID;
    private GroupID groupID;

    public AddAccountToGroupRequestDTO(Denomination denomination, Description description, PersonID personID, GroupID groupID) {
        this.denomination = denomination;
        this.description = description;
        this.personID = personID;
        this.groupID = groupID;
    }

    public Denomination getDenomination() {
        return denomination;
    }

    public void setDenomination(Denomination denomination) {
        this.denomination = denomination;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public PersonID getPersonID() {
        return personID;
    }

    public void setPersonID(PersonID personID) {
        this.personID = personID;
    }

    public GroupID getGroupID() {
        return groupID;
    }

    public void setGroupID(GroupID groupID) {
        this.groupID = groupID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddAccountToGroupRequestDTO that = (AddAccountToGroupRequestDTO) o;
        return Objects.equals(denomination, that.denomination) &&
                Objects.equals(description, that.description) &&
                Objects.equals(personID, that.personID) &&
                Objects.equals(groupID, that.groupID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(denomination, description, personID, groupID);
    }
}

package project.dto;

import java.util.Objects;

public class IsSiblingRequestInfoDTO {

    private String personID1;
    private String personID2;

    public IsSiblingRequestInfoDTO(String personID1, String personID2) {
        this.personID1 = personID1;
        this.personID2 = personID2;
    }

    public String getPersonID1() {
        return personID1;
    }

    public void setPersonID1(String personID1) {
        this.personID1 = personID1;
    }

    public String getPersonID2() {
        return personID2;
    }

    public void setPersonID2(String personID2) {
        this.personID2 = personID2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IsSiblingRequestInfoDTO)) return false;
        IsSiblingRequestInfoDTO that = (IsSiblingRequestInfoDTO) o;
        return Objects.equals(personID1, that.personID1) &&
                Objects.equals(personID2, that.personID2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personID1, personID2);
    }
}